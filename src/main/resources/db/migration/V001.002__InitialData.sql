-- Создаем первого, специального пользователя-анонима.
	-- Пустой список заказов для клиента
	insert into OrderList(id) values (nextval('orderlist_id_seq'));
insert into Client(client_name, order_list_id) values('Неизвестный клиент', currval('orderlist_id_seq'));

insert into Users(login, full_name, password) values( 'user1', 'Первый пользователь', 'a');
insert into Users(login, full_name, password) values( 'user2', 'Второй пользователь', 'b');
