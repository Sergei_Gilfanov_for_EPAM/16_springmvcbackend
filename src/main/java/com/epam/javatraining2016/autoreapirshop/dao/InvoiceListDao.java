package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class InvoiceListDao {
  private InvoiceListMapper mapper;

  public InvoiceListDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(InvoiceListMapper.class);
  }

  public InvoiceListRow create() {
    InvoiceListRow retval = new InvoiceListRow(0);
    mapper.insert(retval);
    return retval;
  }

  public InvoiceListRow get(int invoiceListId) {
    InvoiceListRow retval = mapper.select(invoiceListId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
