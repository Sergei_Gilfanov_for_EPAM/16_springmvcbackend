package com.epam.javatraining2016.autoreapirshop.dao;

public class OrderHistoryRecordCommentRow extends OrderHistoryRecordRow {
  private CommentRow comment;

  public OrderHistoryRecordCommentRow(Integer id) {
    super(id);
  }

  public CommentRow getComment() {
    return comment;
  }

  public void setComment(CommentRow comment) {
    this.comment = comment;
  }
}
