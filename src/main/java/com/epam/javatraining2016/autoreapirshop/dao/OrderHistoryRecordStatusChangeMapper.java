package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

public interface OrderHistoryRecordStatusChangeMapper {
  void insert(OrderHistoryRecordStatusChangeRow orderHistortyRow);

  OrderHistoryRecordStatusChangeRow selectShallow(int recordId);

  OrderHistoryRecordStatusChangeRow selectDeep(int recordId);

  List<OrderHistoryRecordStatusChangeRow> selectForOrderDeep(int orderId);
}
