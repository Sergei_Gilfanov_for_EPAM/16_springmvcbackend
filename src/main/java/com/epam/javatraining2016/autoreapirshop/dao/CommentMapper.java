package com.epam.javatraining2016.autoreapirshop.dao;

public interface CommentMapper {
  void insert(CommentRow comment);

  CommentRow select(int id);
}
