package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderHistoryRecordDao {

  OrderHistoryRecordMapper mapper;

  public OrderHistoryRecordDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(OrderHistoryRecordMapper.class);
  }

  public OrderHistoryRecordRow create(OrderHistoryRow orderHistory) {
    OrderHistoryRecordRow retval = new OrderHistoryRecordRow(0);
    retval.setOrderHistory(orderHistory);
    mapper.insert(retval);
    return retval;
  }

  public OrderHistoryRecordRow getShallow(int orderHitoryRecordId) {
    OrderHistoryRecordRow retval = mapper.selectShallow(orderHitoryRecordId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }
}
