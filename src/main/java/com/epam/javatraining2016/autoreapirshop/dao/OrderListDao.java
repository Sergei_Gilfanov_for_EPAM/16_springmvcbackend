package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class OrderListDao {
  private OrderListMapper mapper;

  public OrderListDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(OrderListMapper.class);
  }

  public OrderListRow create() {
    OrderListRow retval = new OrderListRow(0);
    mapper.insert(retval);
    return retval;
  }

  public OrderListRow get(int orderListId) {
    OrderListRow retval = mapper.select(orderListId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public void add(OrderListRow orderList, OrderRow order) {
    mapper.add(orderList, order);
  }
}
