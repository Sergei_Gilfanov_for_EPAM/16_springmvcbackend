package com.epam.javatraining2016.autoreapirshop.dao;

public class OrderRow extends Row {
  private InvoiceListRow invoiceList;
  private OrderHistoryRow orderHistory;

  OrderRow(Integer id) {
    super(id);
  }

  public InvoiceListRow getInvoiceList() {
    return invoiceList;
  }

  public void setInvoiceList(InvoiceListRow invoiceList) {
    this.invoiceList = invoiceList;
  }

  public OrderHistoryRow getOrderHistory() {
    return orderHistory;
  }

  public void setOrderHistory(OrderHistoryRow orderHistory) {
    this.orderHistory = orderHistory;
  }

}
