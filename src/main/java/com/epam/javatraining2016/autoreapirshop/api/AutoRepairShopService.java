package com.epam.javatraining2016.autoreapirshop.api;

import com.epam.javatraining2016.autoreapirshop.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.protocol.OrdersPaged;
import com.epam.javatraining2016.autoreapirshop.protocol.UserSearchResult;

public interface AutoRepairShopService {
  public String getVersion();

  public int createOrder(int clientId, String commentText);

  public Order getOrder(int orderId);

  public Order[] getOrdersForClient(int clientId);
  public OrdersPaged getOrdersForClient(int clientId, int fromOrder, int pageLength, Integer statusId);

  public int changeOrderStatus(int orderId, String statusName, String commentText);

  public int createClient(String name);

  public Client getClient(int clientId);

  public Client[] getClients();

  public int createComment(int orderId, String commentText);

  public Comment getComment(int commentId);

  public OrderHistoryRecord[] getHistory(int id);

  public UserSearchResult login(String loginName, String password);

  public boolean loginAvailable(String loginName);

  public int createUser(String login, String fullName, String password);
}
